// Одна функция конструктор может расширять/дополнять функциональность своими методами другую функцию, 
// т.е. имея в прототипе одной функции методы, при наследовании этой функции другой - все эти методы 
// передадутся и ей. Устанавливается .prototype наследуемой функции  в .prototype её функции-родителя, 
// если метод не найден в прототипе наследуемой функции JS берёт его из прототипа родителя.


class Employee {
    constructor(name, age, salary) {
        this.name = name
        this.age = age
        this.salary = salary
    }
    set name(name) {
        this._name = name
    }
    get name() {
        return this._name;
    }
    set age(age) {
        this._age = age
    }
    get age() {
        return this._age;
    }
    set salary(salary) {
        this._salary = salary
    }
    get salary() {
        return this._salary 
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary)
        this.lang = lang
    }
    set salary(salary) {
        this._salary = salary * 3
    }
    get salary() {
        return this._salary 
    }
}

let john = new Programmer("john", "20y.o.", 2, "ruby")
console.log(john)
let adam = new Programmer("adam", "16y.o.", 500, "Java")
console.log(adam)
